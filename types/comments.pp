# @summary testing comments in a data type
type Stringstest::Comments = Struct[{
  # this is a comment
  'comment' => String,
}]
